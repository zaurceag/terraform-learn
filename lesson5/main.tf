provider "aws" {}


data "aws_availability_zones" "my-zones" {}
data "aws_region" "current" {}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "Developer"
  }
}

resource "aws_subnet" "corp_subnet" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  availability_zone = data.aws_availability_zones.my-zones.names[0]
  tags = {
      Name : "corp Subnet"

  }

}
output "my_new_aws_subnet" {
    value = aws_subnet.corp_subnet.id
}

output "aws_availability_zones"{
    value = data.aws_availability_zones.my-zones.names
}

output "aws_regions" {
    value = data.aws_region.current.name
}

output "aws_vpc" {
    value = aws_vpc.main.id
}