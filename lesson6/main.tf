# search for latest linux and ubuntu images

provider "aws" {}
  

data "aws_ami" "latest-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]

    }
}

data "aws_ami" "latest-suse-image" {
    most_recent = true
    owners = ["013907871322"]
    filter {
        name = "name"
        values = ["suse-sles-15-sp3-*-hvm-ssd-arm64"]

    }
}

data "aws_ami" "latest-windows-image" {
    most_recent = true
    owners = ["801119661308"]
    filter {
        name = "name"
        values = ["Windows_Server-2019-English-Full-Base-*"]

    }
}

output "latest_linux_id" {
    value = data.aws_ami.latest-linux-image.image_id
}



output "latest_suse_id" {
    value = data.aws_ami.latest-suse-image.image_id
}

output "latest_windows_id" {
    value = data.aws_ami.latest-windows-image.image_id
}
