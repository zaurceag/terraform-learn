#---------------------------------------------------------------------
#
# Create instances in a different regions in the same main.tf
#
#
#----------------------------------------------------------------------


provider "aws" {
    region = "us-east-1"
}

provider "aws" {
    region = "ca-central-1"
    alias = "canada"
}

provider "aws" {
    region = "eu-central-1"
    alias = "ger"
}


resource "aws_instance" "inst1" {
    ami   = "ami-065efef2c739d613b"
    instance_type = "t3.medium"
    tags = {
        Name = "USA-server"
    }
}

data "aws_ami" "latest-amazon-linux" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amz*"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}
    resource "aws_instance" "inst2" {
    provider = aws.canada
    ami   = "ami-04c12937e87474def"
    instance_type = "t3.large"
    tags = {
        Name = "CANADA-server"
    }
    }

        resource "aws_instance" "inst3" {
    provider = aws.ger
    ami   = data.aws_ami.latest-amazon-linux.id
    instance_type = "t3.small"
    tags = {
        Name = "GERMANY-server"
    }
    }