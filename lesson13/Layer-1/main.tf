#---------------------------------------------------------------------
#
# Remote State
# S3 bucket created by gui
#
#----------------------------------------------------------------------


provider "aws" {
    region = "eu-central-1"
}

terraform {
    backend "s3" {
      bucket = "zaur-guliyev-zeca-s3bucket"
      key = "dev/network/terraform.tfstate"
      region = "us-east-1"
    }
    
}
#---------------------------------------------------------

variable "vpc_cidr" {
    default = "10.0.0.0/16"
}

variable "env" {
    default ="dev"
}

variable "public_sunet_cidrs" {
    default = [
        "10.0.1.0/24",
        "10.0.2.0/24",
    ]
       
    
}


data "aws_availability_zones" "available" {}

resource "aws_vpc" "main" {
    cidr_block = var.vpc_cidr
    tags = {
        Name = "${var.env}-VPC"
    }
}

resource "aws_internet_gateway" "main" {
    vpc_id = aws_vpc.main.id
    tags = {
        Name = "${var.env}-igr"
    }
}

resource "aws_subnet" "pub_subnets" {
  count = length(var.public_sunet_cidrs)
  vpc_id     = aws_vpc.main.id
  cidr_block = element(var.public_sunet_cidrs ,count.index)
  availability_zone = data.aws_availability_zones.available.names[count.index]
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.env} -public ${count.index +1}"
  }
}

resource "aws_route_table" "example" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = "${var.env}-route"
  }
}
resource "aws_route_table_association" "public_routes"{
    count = length(aws_subnet.pub_subnets[*].id)
    route_table_id = aws_route_table.example.id
    subnet_id = element(aws_subnet.pub_subnets[*].id, count.index)
}




output "vpc_id" {
    value = aws_vpc.main.id
}

output "vpc_cidr" {
    value = aws_vpc.main.cidr_block
}

output "aval_zone" {
    value = data.aws_availability_zones.available.names
}

output "public_sub_ids" {
    value = aws_subnet.pub_subnets[*].id
}
