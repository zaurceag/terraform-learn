provider "aws" {
    region = "ca-central-1"
}
terraform {
    backend "s3" {
      bucket = "zaur-guliyev-zeca-s3bucket"
      key = "dev/layer2/terraform.tfstate"
      region = "us-east-1"
    }
    
}
variable "allow_ports" {
    description = "Enter list of ports that you desire to listen"
    default = ["80","22","8080"]
}

variable "instance_type" {
    default = "t3.micro"
}


data "terraform_remote_state" "network" {
    backend = "s3"
    config = {
      bucket = "zaur-guliyev-zeca-s3bucket"
      key = "dev/network/terraform.tfstate"
      region = "us-east-1" 
    }
}

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amz*"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}



resource "aws_instance" "myapp-server" {
    ami = data.aws_ami.latest-amazon-linux-image.id
    instance_type = var.instance_type
    subnet_id = data.terraform_remote_state.network.outputs.public_sub_ids[0]
    vpc_security_group_ids = [aws_security_group.my-sg.id]
    availability_zone = data.terraform_remote_state.network.outputs.aval_zone[1]
    user_data = <<EOF
    #!/bin/bash
    sudo yum -y update
    sudo yum -y install httpd
    myip='curl http://169.254.169.254/latest/meta-data/local-ipv4'
    sudo echo "Salam Jamil ve Aylin,both from USA" > index.html
    sudo cp index.html /var/www/html/
    sudo service httpd start
EOF
}

resource "aws_security_group" "my-sg"{
  name = "Security Group"
  vpc_id = data.terraform_remote_state.network.outputs.vpc_id

  dynamic "ingress" {
    for_each = var.allow_ports
    content {
        from_port   = ingress.value
        to_port     = ingress.value
        protocol    = "tcp"
        cidr_blocks = [data.terraform_remote_state.network.outputs.vpc_cidr]
     #   cidr_blocks = ["0.0.0.0/0"]  
    }
  
  }
        egress {
            from_port   = 0
            to_port     = 0
            protocol    = "-1"
            cidr_blocks = ["0.0.0.0/0"]
        }
        tags = {
            Name    = "Web Server Security Group "
            Owner   = "Zaur Guliyev"
            Project = "Mobile apps"
        }
}

output "netwrok_state" {
    value = data.terraform_remote_state.network
}

output "ter-sg_id" {
    value = aws_security_group.my-sg.id
}

output "public_ip" {
    value = aws_instance.myapp-server.public_ip
}