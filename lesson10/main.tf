#---------------------------------------------------------------------
#
# Conditions
#
#
#----------------------------------------------------------------------


provider "aws" {
    region = "us-east-2"
}


variable "env" {
    default = "prod"
}

variable "prod_owner" {
    default = "Zaur Guliyev"
}

variable "noprod_owner" {
    default = "anonymous"
}

resource "aws_instance" "web" {
  ami           = "ami-07251f912d2a831a3"
  instance_type = var.env == "prod" ? "t2.large" : "t2.micro"

  tags = {
    Name = "$(var.env)-server"
    Owner = var.env == "prod" ? "var.prod_owner" : "var.noprod_owner"
  }
}

resource "aws_instance" "app" {
    count = var.env == "prod" ? 1 : 0
  ami           = "ami-07251f912d2a831a3"
  instance_type = var.env == "prod" ? "t2.large" : "t2.micro"

  tags = {
    Name = "$(var.env)-app"
   
  }
}