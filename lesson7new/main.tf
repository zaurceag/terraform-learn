# Provision Highly available Web in any region Default VPC
# Create :
# Security Group
# Launch Confirguration with Auto AMI Lookup
# Auto Scaling Group using 2 Availability Zones
# Classic Load Balancer in 2 Availability Zones
# vpc_zone_identifier -> bunu aws_default_subnet edib onun resourcelarini yaziriq

#--------------------------------------------------------------------

provider "aws" {
    region = "us-west-1"
}

data "aws_availability_zones" "available" {}
data "aws_ami" "latest-linux-image" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]

    }
}

resource "aws_security_group" "web-sg" {
  name        = "Dynamic Security Group"

    dynamic "ingress" {
        for_each =  ["80","22"]
        content {
            from_port        = ingress.value
            to_port          = ingress.value
            protocol         = "tcp"
            cidr_blocks      = ["0.0.0.0/0"]   
        }
    }
 

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
 
  }


}

resource "aws_launch_configuration" "web" {
    name_prefix = "WebServer-HA-"
    image_id = data.aws_ami.latest-linux-image.id
    instance_type = "t3.micro"
    security_groups = [aws_security_group.web-sg.id]
    user_data = file("user_data.sh")

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_autoscaling_group" "web" {
  name                     = "ASG-${aws_launch_configuration.web.name}"
  max_size                  = 2
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 2
  force_delete              = true
  launch_configuration      = aws_launch_configuration.web.name
  vpc_zone_identifier       = [aws_default_subnet.default_az1.id, aws_default_subnet.default_az2.id]
  load_balancers            = [aws_elb.web.name]



  tag {
    key                 = "Name"
    value               = "WebServer-in ASG"
    propagate_at_launch = true
  }

}

resource "aws_elb" "web" {
    name = "WebServer-HA-ELB"
    availability_zones = [data.aws_availability_zones.available.names[0],data.aws_availability_zones.available.names[1]]
    security_groups  =   [aws_security_group.web-sg.id]
    listener {
        lb_port = 80
        lb_protocol = "http"
        instance_port = 80
        instance_protocol = "http"
    }
    health_check {
        healthy_threshold = 2
        unhealthy_threshold = 2
        timeout = 8
        target = "HTTP:80/"
        interval = 10
    }
    tags = {
        Name = "WebServer-HA-ELB"
    }
}
resource "aws_default_subnet" "default_az1" {
  availability_zone = data.aws_availability_zones.available.names[0]

  tags = {
    Name = "Default subnet for us-east-1a"
  }
}

resource "aws_default_subnet" "default_az2" {
  availability_zone = data.aws_availability_zones.available.names[1]

  tags = {
    Name = "Default subnet for us-east-1b"
  }
}

output "aws_lb_url" {
    value = aws_elb.web.dns_name
}