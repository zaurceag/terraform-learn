output "db_public_ip" {
    value = aws_instance.aws-db.public_ip
}

output "app_public_ip" {
    value = aws_instance.aws-app.public_ip
}

output "web_public_ip" {
    value = aws_instance.aws-web.public_ip
}