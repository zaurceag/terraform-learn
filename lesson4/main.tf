provider "aws"{
    region = "eu-west-1"
}
resource "aws_eip" "my_static_ip" {      
  instance = aws_instance.aws-web.id
}
resource "aws_eip" "my_static_ip_app" {
  instance = aws_instance.aws-app.id
}
resource "aws_eip" "my_static_ip_db" {
  instance = aws_instance.aws-db.id
}
resource "aws_instance" "aws-web" {
    ami = "ami-0d71ea30463e0ff8d"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.Dinamo-sg.id]
    user_data = file("user_data.sh")
    key_name = "Ireland"
    tags = {
    Name = "WEB"
  }
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_instance.aws-db]
}
resource "aws_instance" "aws-app" {
    ami = "ami-0d71ea30463e0ff8d"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.Dinamo-sg.id]
    user_data = file("user_data.sh")
    key_name = "Ireland"
    tags = {
    Name = "APP"
  }
  lifecycle {
    create_before_destroy = true
  }
  depends_on = [aws_instance.aws-db]
}
resource "aws_instance" "aws-db" {
    ami = "ami-0d71ea30463e0ff8d"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.Dinamo-sg.id]
    user_data = file("user_data.sh")
    key_name = "Ireland"
    tags = {
    Name = "DB"
  }
  lifecycle {
    create_before_destroy = true
  }
}
/*resource "aws_key_pair" "ssh-key"{
    key_name = "server-key"
    public_key = "${file(var.my_public_location)}"

}*/

resource "aws_security_group" "Dinamo-sg" {
  #name        = "Default Security Group"

    dynamic "ingress" {
        for_each =  ["80","443","1521"]
        content {
            from_port        = ingress.value
            to_port          = ingress.value
            protocol         = "tcp"
            cidr_blocks      = ["0.0.0.0/0"]   
        }
    }
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
 
  }


}
