#----------------------------------------
# Creating:
# new vpc,subnet,route table,internet gateway,security group,
# elastic ip,aws instance,

provider "aws"{
  region = "eu-central-1"
}
data "aws_availability_zones" "current" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}
resource "aws_vpc" "main_co_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "RootVPC"
  }
}

resource "aws_subnet" "new_subnet2016" {
  vpc_id     = aws_vpc.main_co_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = data.aws_availability_zones.current.names[0]
  tags = {
      Name : "Root Subnet"
  }
  }


  resource "aws_route_table" "myapp-route-table"{
    vpc_id = aws_vpc.main_co_vpc.id

    route{
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
      Name = "RootRouteTable"
    }
}
resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.main_co_vpc.id
  tags = {
    Name = "RootInternetGateway"
  }  
}

resource "aws_security_group" "test-sg" {
      name = "Test Security Group"
      vpc_id = aws_vpc.main_co_vpc.id

    dynamic "ingress" {
        for_each =  ["80","22"]
        content {
            from_port        = ingress.value
            to_port          = ingress.value
            protocol         = "tcp"
            cidr_blocks      = ["0.0.0.0/0"]   
        }
    }
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
 
  }

tags= {
  Name = "RootSecurityGroup"
}

}

resource "aws_eip" "lb" {
  instance = aws_instance.webserver.id
  vpc      = true
}

resource "aws_eip" "lb1" {
  instance = aws_instance.database.id
  vpc      = true
}
resource "aws_instance" "webserver" {
    ami = "ami-094c442a8e9a67935"
    instance_type = "t3.micro"
    subnet_id = aws_subnet.new_subnet2016.id
    availability_zone = "eu-central-1a"
    vpc_security_group_ids = [aws_security_group.test-sg.id]
    associate_public_ip_address = true
    user_data = file("user_data.sh")
    tags = {
    Name = "instance1"
  }
  lifecycle {
    create_before_destroy = true
  }

}

resource "aws_instance" "database" {
    ami = "ami-094c442a8e9a67935"
    instance_type = "t3.micro"
    availability_zone = "eu-central-1a"
    subnet_id = aws_subnet.new_subnet2016.id
    vpc_security_group_ids = [aws_security_group.test-sg.id]
    associate_public_ip_address = true
    user_data = file("user_data.sh")
    tags = {
    Name = "instance2"
  }
  lifecycle {
    create_before_destroy = true
  }
}



