output "webserver_instance1_id"{
  value = aws_instance.webserver.public_ip
}

output "webserver_instance2_id"{
  value = aws_instance.database.public_ip
}

/*output "public_ip1" {
  value = aws_eip.my_static_ip1.public_ip
}*/
/*output "public_ip2" {
  value = aws_eip.my_static_ip2.public_ip
}*/

output "aws_security_group" {
    value = aws_security_group.test-sg.id
}

output "available_zones" {
  value = data.aws_availability_zones.current.names
}

output "target_groups" {
  value = aws_lb_target_group.test.name
}

output "lb_url" {
  value = aws_elb.web.name
}