variable "vpc_cidr_block" {
    default = "10.0.0.0/16"
}

variable "avail_zone" {
    default = "ap-southeast-2a"
}

variable "env_prefix" {
    default = "dev"
}

variable "instance_type" {
    default = "t3.micro"
}

variable "subnet_cidr_block" {
    default = "10.0.10.0/24"
}

variable "my_public_location" {
    default = "C:/Users/zaur.guliyev/.ssh/id_rsa.pub"
}