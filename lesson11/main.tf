#---------------------------------------------------------------------
#
# User create,Count
#
#
#----------------------------------------------------------------------

provider "aws" {
    region = "us-east-2"
}

variable "aws_users" {
    description = "List of IAM users"
    default = ["zaur","muwfig","elnara","cabbar","rzaxan","yolchu","ahmed","memmed"]
}


resource "aws_iam_user" "user1" {
  name = "devops"
}


resource "aws_iam_user" "users" {
    count = length(var.aws_users)
    name = element(var.aws_users, count.index)
}

output "created_iam_users_id" {
    value = aws_iam_user.users[*].id
}

#-------------------------------------------------------------------------


resource "aws_instance" "servers" {
    count = 3
    ami   = "ami-07251f912d2a831a3"
    instance_type = "t3.micro"
    tags = {
        Name = "Server Number ${count.index  +1}"
    }

}
// Print nice MAY of INSTANCE ID
output "server_all" {
    value = {
        for server in aws_instance.servers :
        server.id => server.public_ip
    }
}