provider "aws"{
    region = "us-west-2"
}

resource "aws_instance" "aws-web" {
    ami = "ami-098e42ae54c764c35"
    instance_type = "t3.micro"
    vpc_security_group_ids = [aws_security_group.my-seguritygroup.id]
    user_data = file("user_data.sh")
    tags = {
    Name = "HelloBRO"
  }
}


resource "aws_security_group" "my-seguritygroup" {
  name        = "My Security Group"

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
 
  }


}