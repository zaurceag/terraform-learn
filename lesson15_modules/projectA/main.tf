provider "aws" {
    region = "eu-north-1"
}

module "vpc_dev" {
    source = "../aws_network"
    env = "development"
    vpc_cidr = "10.100.0.0/16"
    public_subnet_cidrs = ["10.100.1.0/24","10.100.2.0/24"]
    private_subnet_cidrs = ["10.100.11.0/24","10.100.22.0/24"]
}

module "vpc_lab" {
    source = "../aws_network"
    env = "laboratory"
    vpc_cidr = "10.10.0.0/16"
    public_subnet_cidrs = ["10.10.1.0/24","10.10.2.0/24"]
    private_subnet_cidrs = ["10.10.55.0/24","10.10.33.0/24"]

}

output "dev_public_sub_ids" {
    value = module.vpc_dev.public_subnet_ids
}

output "dev_private_sub_ids" {
    value = module.vpc_dev.private_subnet_ids
}
