#------------------------------------------------------
#
# Create 3 ubuntu servers(ec2) in 3 different regions
# 
#
#-------------------------------------------------------



terraform {
    required_providers {
      aws = {
        source = "hashicorp/aws"
        configuration_aliases = [
        aws.dev,
        aws.prod,
        aws.root
        ]
      }
    }
    
}





data "aws_ami" "latest-ubuntu-root" {
    provider = aws.root
    most_recent = true
    owners = ["099720109477"]
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server*"]
    }
   
}

data "aws_ami" "latest-ubuntu-dev" {
    provider = aws.dev
    most_recent = true
    owners = ["099720109477"]
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server*"]
    }
    
}

data "aws_ami" "latest-ubuntu-prod" {
    provider = aws.prod
    most_recent = true
    owners = ["099720109477"]
    filter {
        name = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server*"]
    }
  
}

resource "aws_instance" "my-instance-root" {
    provider = aws.root
    ami = data.aws_ami.latest-ubuntu-root.id
    instance_type = var.instance_type
        tags = {
        Name = "Root-server"
    }
}

resource "aws_instance" "my-instance-dev" {
    provider = aws.dev
    ami = data.aws_ami.latest-ubuntu-dev.id
    instance_type = var.instance_type
        tags = {
        Name = "Dev-server"
    }
}

resource "aws_instance" "my-instance-prod" {
    provider = aws.prod
    ami = data.aws_ami.latest-ubuntu-prod.id
    instance_type = var.instance_type
        tags = {
        Name = "Prod-server"
    }
}

