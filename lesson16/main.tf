provider "aws" {
    region = "eu-north-1"
}

provider "aws" {
    region = "us-west-1"
    alias = "dev"

    assume_role {
    role_arn = "arn:aws:iam::901081313376:role/TerraformRole"
}
}

provider "aws" {
    region = "us-west-2"
    alias = "prod"
    
    assume_role {
    role_arn = "arn:aws:iam::901081313376:role/TerraformRole"
}
}


module "servers" {
    source = "./module_servers"
    instance_type = "t3.small"
    providers = {
        aws.dev = aws.dev
        aws.prod = aws.prod
        aws.root = aws
        
        
    }
}
