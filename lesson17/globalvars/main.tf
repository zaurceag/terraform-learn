#-----------------------------------------------------------
#
# Global variables in remote state  on s3
#
#
#-----------------------------------------------------------
provider "aws" {
    region = "eu-central-1"
}

terraform {
    backend "s3" {
      bucket = "zaur-guliyev-zeca-s3bucket"
      key = "globalvars/terraform.tfstate"
      region = "us-east-1"
    }
    
}

output "company_name" {
    value = "Idrak Technology transfer"
}

output "pki_engineer" {
    value = "Zaur Guliyev"
}

output "tags" {
    value = {
        Project = "EID"
        ConsCenter = "R&D"
        Country = "Canada"
    }
}