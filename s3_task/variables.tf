variable "username" {
  type    =     list
  default = ["messi", "ronaldo"]
}

variable "region" {
  default = "ap-southeast-2"
}