#--------------------------------------
# Variables
#
#
#--------------------------------------

provider "aws" {
region = var.region   
}

data "aws_ami" "latest-amazon-linux" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}

resource "aws_eip" "my_static_ip" {
  instance = aws_instance.myapp-server.id
  vpc      = true
  tags = {
    Name = "Server IP"
    Owner = "Zaur Guliyev"
    Porject = "Mobile apps"
  }
}

resource "aws_instance" "myapp-server" {
    ami = data.aws_ami.latest-amazon-linux.id
    instance_type = var.instance_type
    user_data = file("user_data.sh")
    #subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.my-sg.id]
    #availability_zone = var.avail_zone

}

resource "aws_security_group" "my-sg"{
  name = "My Security Group"

  dynamic "ingress" {
    for_each = var.allow_ports
    content {
        from_port   = ingress.value
        to_port     = ingress.value
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]  
    }
  
  }
        egress {
            from_port   = 0
            to_port     = 0
            protocol    = "-1"
            cidr_blocks = ["0.0.0.0/0"]
        }
        tags = {
            Name    = "Web Server Security Group "
            Owner   = "Zaur Guliyev"
            Project = "Mobile apps"
        }
}