variable "region" {
    description = "Please enter your desired region"
    default = "us-east-1"
    type = string
}
variable "instance_type" {
    description = "Enter valid instance type"
    default = "t3.micro"
    type = string
}

variable "allow_ports" {
    description = "Enter list of ports that you want to listen"
    type = list
    default = ["80","22","8080"]
}
